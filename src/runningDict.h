/* Copyright (c) 2023 Thomas Bailleux
*
*  Licensed under the MIT license: https://opensource.org/licenses/MIT
*  Permission is granted to use, copy, modify, and redistribute the work.
*  Full license information available in the project LICENSE file.*/

#include <map>
#include <string>

/** Object containing the data need to translate into runes
*/

class RunningDict {
public:
	RunningDict(const char *dataFileName);
	void translate(const std::string &source, std::string &outcome);

private:
	std::map<std::string, std::string> dataBase;
	size_t maxSizeTranslation;
};
