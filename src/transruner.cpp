/* Copyright (c) 2023 Thomas Bailleux
*
*  Licensed under the MIT license: https://opensource.org/licenses/MIT
*  Permission is granted to use, copy, modify, and redistribute the work.
*  Full license information available in the project LICENSE file.*/

#include <iostream>
#include <string>
#include <stdexcept>

#include "runningDict.h"

/** Transform the given command line argument into a single String
**/
std::string extractArg(int argc, char **argv);

/** Open the data file given, and use it to change the text
**/
std::string transgrifRunic(RunningDict translator, const std::string &inputStr);

int main(int argc, char **argv) {
	try {
		if (argc == 1) {  // arg 2 is the mandatory dictionnary path : raise error if not provided
			throw std::invalid_argument("Dictionnary path is mandatory");
		}
		RunningDict translator{argv[1]};  // Setup RunningDict
	
		if (argc == 2) {  // check standard input if no text provided (only dict path)
			bool first_line = true;
			for (std::string line; std::getline(std::cin, line);) {
				if (first_line) {
					first_line = false;
				}
				else std::cout << "\n";
				std::cout << transgrifRunic(translator, line);
			}
			return 0;
		}

		if (std::string inputStr = extractArg(argc, argv); !inputStr.empty())
			std::cout << transgrifRunic(translator, inputStr) + "\n";
		return 0;
	} catch (...) {
		throw;
	}
}


std::string transgrifRunic(RunningDict translator, const std::string &inputStr) {
	std::string runicVersion;
	translator.translate(inputStr, runicVersion);
	return runicVersion;
}


std::string extractArg(int argc, char **argv) {
	if (argc > 2) {  // concatenates everything past the second arg
		std::string inputStr = argv[2];
		for (int i = 3; i < argc; ++i) {
			inputStr = inputStr + ' ' + argv[i];
		}
		return inputStr;
	}
	return {};
}