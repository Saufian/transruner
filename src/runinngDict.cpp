/* Copyright (c) 2023 Thomas Bailleux
*
*  Licensed under the MIT license: https://opensource.org/licenses/MIT
*  Permission is granted to use, copy, modify, and redistribute the work.
*  Full license information available in the project LICENSE file.*/

#include "runningDict.h"

#include <fstream>
#include <string>
#include <algorithm>
#include <stdexcept>

/** Open , check data file and return content
**/
static std::string openFile(const char *fileName) {
	std::fstream file;
	file.open(fileName, std::ios::in);
	if (!file) {
		throw std::invalid_argument("No file at this path");
	}
	else {  // extract file content
		std::string content;
		char str;
		while(file.get(str)) {
			content += str;
		}
		
		file.close();
		return content;
	}
}


/** Main constructor
* Open and format the given file content to build a map that will be used later
* Also find "maxSizeTranslation"
*/
RunningDict::RunningDict(const char *dataFileName) {
	std::string input = "";
	try {input = openFile(dataFileName);}
	catch (...) {throw;}
	
	size_t beginCursor = 0;
	size_t endCursor = 0;
	maxSizeTranslation = 0;

	while(beginCursor < input.size()) {
		int tab = input.find('\t', beginCursor);
		endCursor = (input.find('\n', beginCursor) != std::string::npos) ? input.find('\n', beginCursor) : input.size();
		
		dataBase[input.substr(beginCursor, tab - beginCursor)] = input.substr(tab + 1, endCursor - tab - 1);
		maxSizeTranslation = (maxSizeTranslation > tab - beginCursor) ? maxSizeTranslation : (tab - beginCursor);
		
		beginCursor = endCursor+1;
	}
}

/** Use the constructed map to translate a source text
*/
void RunningDict::translate(const std::string &source, std::string &outcome) {
	outcome = source.substr();
	std::transform(outcome.begin(), outcome.end(), outcome.begin(), ::tolower);
	for (int size = maxSizeTranslation; size > 0; size--) {
		size_t cursor = 0;
		while (cursor < outcome.size()-size+1) {
			if (std::string transl = dataBase[outcome.substr(cursor, size)]; transl != "") {
				outcome.erase(cursor, size);
				outcome.insert(cursor, transl);
			}
			cursor++;
		}
	}
}

