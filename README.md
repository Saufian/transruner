# Count characters per line

A small script using a basic dictionary to write sentences with runes according
to phonetics pronunciation.

## Quick start

### Download

Get the project by downloading directly
[the source code on GitLab](https://gitlab.com/Saufian/ccpl/-/archive/master/transruner.zip)

Or clone the repository:

```bash
git clone https://gitlab.com/Saufian/transruner.git
```

### Build / Run

Building the tool is as simple as running the `make` command (no other
pre-requisite):

1. unpack the source code in a folder
2. enter into this folder
3. run the `make` command

### Install and Usage

Once built, `transruner` can be call in it local folder, or add to your *path*.
You can find the executable in the `obj/` folder.

You will need to provided the path to a dictionary file to use when calling
`transruner`. A basic one can be found in the `data/` folder of this project
(used to pass from French to runes)

Example of use (when being at the root of this project):

```shell
$ cat test/quote.tmpl | bin/transruner data/fr-rune.txt; echo ""
ᚨ.ᚹᛇᚾᚲᚱᛖ.ᛊᚨᚾᛊ.ᛈᛖᚱᛁᛚ,.ᛟᚾ.ᛏᚱᛁᛟᛗᚠᛖ.ᛊᚨᚾᛊ.ᚷᛚᛟᛁᚱᛖ.
```

```shell
$ bin/transruner data/fr-rune.txt "A vaincre sans peril, on triomphe sans gloire."
ᚨ.ᚹᛇᚾᚲᚱᛖ.ᛊᚨᚾᛊ.ᛈᛖᚱᛁᛚ,.ᛟᚾ.ᛏᚱᛁᛟᛗᚠᛖ.ᛊᚨᚾᛊ.ᚷᛚᛟᛁᚱᛖ.
```

### Create your own dictionary file

The file used as a dictionary to transform words is a text file built as follows:

- each line delimits an entry
- on the left there are the characters to be replaced
- on the right the replacement string
- each part are separated by a tab

Exemple

```txt
au	ᛟ
```

## License

Copyright (c) 2023 Thomas Bailleux

This code is released under the MIT license. See the [LICENSE](LICENSE) file for
more information.

## Contact

You can contact the developer by opening an issue in the project repository.

Homepage: [Transruner](https://gitlab.com/Saufian/transruner)
